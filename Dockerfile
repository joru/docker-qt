FROM base/devel
RUN pacman -Sy --noconfirm \
  qt5-tools \
  qt5-quickcontrols \
  qt5-quickcontrols2 \
  qt5-webengine \
  qt5-declarative \
  clang=7.0.1
